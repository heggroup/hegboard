#!/bin/bash

set -e
curl -Lf --header "PRIVATE-TOKEN: $HEGBOARD_GITLAB_CI_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/33064285/jobs/artifacts/main/download?job=deploy-job" --output hegboard-web.zip
unzip hegboard-web.zip
rm hegboard-web.zip
rm -rf static
mv build static