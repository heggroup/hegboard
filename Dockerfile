FROM balenalib/raspberrypi4-64-debian

ARG TOKEN
RUN sudo apt update
RUN sudo apt install -y curl unzip gcc libssl-dev pkg-config libc-dev git g++ make
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain none -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN rustup install stable
RUN rustup default stable
RUN rustup component add cargo rustfmt
RUN git clone https://github.com/hzeller/rpi-rgb-led-matrix.git
WORKDIR /
COPY . .
ENV HEGBOARD_GITLAB_CI_ACCESS_TOKEN=$TOKEN
RUN scripts/update-web.sh
RUN cargo build --all-features
ENTRYPOINT [ "target/release/hegboard" ]