use std::{
    fs::remove_file,
    process::{Child, Command},
    thread,
    time::{Duration, Instant},
};

use portpicker::pick_unused_port;

pub struct HegBoardServer {
    pub process: Child,
    pub port: u16,
    pub url: String,
    pub path: String,
}

impl Drop for HegBoardServer {
    fn drop(&mut self) {
        println!("stopping server");
        self.process.kill().expect("Server already shutdown!");
        self.process.wait().expect("Failed to shutdown server!");
        remove_file(&self.path).expect("Failed to cleanup logfile");
    }
}

impl HegBoardServer {
    pub async fn new() -> Self {
        println!("starting server");
        let port: u16 = pick_unused_port().expect("No ports free");
        let path = format!("log-{}", port.to_string());
        let server = HegBoardServer {
            process: Command::new("sh")
                .arg("-c")
                .env_clear()
                .env("HEGBOARD_PORT", port.to_string())
                .env("HEGBOARD_LOGNAME", &path)
                .arg("target/debug/hegboard")
                .spawn()
                .expect("Failed to start server!"),
            port,
            url: format!("{}{}", "http://localhost:", port.to_string()),
            path,
        };
        wait_for_healthcheck(&server).await;
        server
    }
}

async fn server_is_not_healthy(server: &HegBoardServer) -> bool {
    match reqwest::get(server.url.to_owned() + "/app/health").await {
        Ok(_) => false,
        Err(_) => true,
    }
}

async fn wait_for_healthcheck(server: &HegBoardServer) {
    let timeout = Duration::from_millis(1000);
    let now = Instant::now();
    while server_is_not_healthy(server).await {
        thread::sleep(Duration::from_millis(10));
        if now.elapsed() >= timeout {
            panic!("Timed out waiting for server to be healthy");
        }
    }
}
