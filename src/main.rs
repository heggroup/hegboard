mod matrix;
mod webserver;

use actix_web;
use std::{env, ffi::OsString, sync::Arc};
use webserver::start_webserver;

#[actix_web::main]
async fn main() {
    std::env::set_var("RUST_LOG", "actix_web=debug, hegboard=info");
    env_logger::init();

    let port: u16 = env::var_os("HEGBOARD_PORT")
        .unwrap_or(OsString::from("43680"))
        .into_string()
        .unwrap()
        .parse::<u16>()
        .unwrap();

    log::info!("HegBoard Online at http://localhost:{}", port.to_string());

    start_webserver(port, Arc::new(matrix::new())).await;
}
