compile:
	cargo build

test:
	cargo test

format:
	cargo fmt

update-web:
	./scripts/update-web.sh

build: update-web compile

run:
	cargo run

docker:
	docker images | grep hegboard | awk '{print $$3}' | xargs docker rmi
	docker build -t hegboard --build-arg TOKEN=${HEGBOARD_GITLAB_CI_ACCESS_TOKEN} .

clean:
	rm -rf target && rm -rf static
	docker image prune

run-docker:
	docker run --rm -d hegboard

clean-docker:
	docker ps -a | awk '{print $$1}' | grep -v CONTAINER  | xargs docker rm
