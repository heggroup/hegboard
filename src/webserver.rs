use std::sync::Arc;

use actix_cors::Cors;
use actix_web::{
    http,
    middleware::Logger,
    web::{self, Data, Json},
    App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use mime_guess::from_path;
use rust_embed::RustEmbed;
use serde::{Deserialize, Serialize};

use crate::matrix::Matrix;

#[derive(RustEmbed)]
#[folder = "static/"]
struct Asset;

#[derive(Deserialize, Serialize)]
pub struct Pixel {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

#[derive(Deserialize, Serialize)]
pub struct Pixels {
    pub pixels: Vec<Vec<Pixel>>,
}

pub async fn start_webserver(port: u16, matrix: Arc<dyn Matrix + Send + Sync + 'static>) {
    let matrix_data = Data::from(matrix);

    HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
            .allowed_header(http::header::CONTENT_TYPE)
            .max_age(3600);

        App::new()
            .service(
                web::scope("/app")
                    .app_data::<Data<(dyn Matrix + Send + Sync)>>(Data::clone(&matrix_data))
                    .route("/health", web::get().to(healthcheck))
                    .route("/version", web::get().to(version))
                    .route("/draw", web::post().to(handle_draw)),
            )
            .service(serve_embedded_file)
            .wrap(cors)
            .wrap(Logger::new("%r %s"))
    })
    .bind(("127.0.0.1", port))
    .expect("Failed to bind to port")
    .run()
    .await
    .expect("Failed to start Webserver!");
}

async fn version() -> impl Responder {
    HttpResponse::Ok().body(env!("CARGO_PKG_VERSION"))
}

async fn healthcheck() -> impl Responder {
    HttpResponse::Ok().body("Hegboard is online")
}

fn is_invalid(pixels: &Json<Pixels>) -> bool {
    if pixels.pixels.len() != 128 {
        true
    } else {
        for col in &pixels.pixels {
            if col.len() != 64 {
                return true;
            }
        }
        false
    }
}

async fn handle_draw(pixels: Json<Pixels>, request: HttpRequest) -> impl Responder {
    if is_invalid(&pixels) {
        HttpResponse::BadRequest()
    } else {
        let matrix = HttpRequest::app_data::<Data<(dyn Matrix + Send + Sync)>>(&request)
            .expect("could not find matrix");
        match matrix.draw_pixels(&pixels) {
            Ok(_) => HttpResponse::Ok(),
            Err(e) => {
                log::error!("Failed to draw: {}", e);
                HttpResponse::InternalServerError()
            }
        }
    }
}

#[actix_web::get("/{_:.*}")]
async fn serve_embedded_file(path: web::Path<String>) -> impl Responder {
    let mut path_str = path.as_str().to_owned();
    if !path_str.contains(".") {
        path_str.push_str("index.html");
    }
    match Asset::get(&path_str) {
        Some(content) => HttpResponse::Ok()
            .content_type(from_path(path_str).first_or_octet_stream().as_ref())
            .body(content.data.into_owned()),
        None => HttpResponse::NotFound().body("404 Not Found"),
    }
}
