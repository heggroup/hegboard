mod server;

use actix_rt;
use reqwest;
use server::HegBoardServer;

#[actix_rt::test]
async fn healthcheck_returns_200() {
    let hegboard: HegBoardServer = HegBoardServer::new().await;
    let result = reqwest::get(hegboard.url.to_owned() + "/app/health").await;
    assert_eq!(
        result.expect("No Result From Health API").status(),
        reqwest::StatusCode::OK
    );
}

#[actix_rt::test]
async fn version_endpoint_returns_version() {
    let hegboard: HegBoardServer = HegBoardServer::new().await;
    let result = reqwest::get(hegboard.url.to_owned() + "/app/version")
        .await
        .expect("No Result From Version Endpoint");
    assert_eq!(result.status(), reqwest::StatusCode::OK);
    assert_eq!(
        result.text().await.expect("No Version Response Body"),
        env!("CARGO_PKG_VERSION")
    );
}
